<?php

/**
 * @file
 * Plugin to provide access control/visibility based on taxonomy section.
 */
$plugin = array(
  'title' => t('Taxonomy section'),
  'description' => t('Control access by taxonomy sections.'),
  'callback' => 'taxonomy_sections_modifier_ctools_access_check',
  'settings form' => 'taxonomy_sections_modifier_ctools_access_settings',
  'summary' => 'taxonomy_sections_modifier_ctools_access_summary',
  //'required context' => new ctools_context_required(t('Node'), 'node'),
);

/**
 * Settings form
 */
function taxonomy_sections_modifier_ctools_access_settings($form, &$form_state, $conf) {
 
  $form['settings']['taxonomy_section'] = array(
    '#type' => 'select',
    '#title' => t('Taxonomy section modifier'),
    '#default_value' => $conf['taxonomy_section'],
    '#options' => taxonomy_sections_get_purl_modifiers(),
  );
  $form['settings']['taxonomy_section_default'] = array(
    '#type' => 'checkbox',
    '#title' => t('Give access without section parameter'),
    '#default_value' => $conf['taxonomy_section_default'],
  );

  return $form;
}

/**
 * Check for access.
 */
function taxonomy_sections_modifier_ctools_access_check($conf, $context) {


  $current_section = taxonomy_sections_get_current_purl_tid();
  if ($current_section) {
    if ($conf['taxonomy_section'] == taxonomy_sections_get_current_purl_tid()) return TRUE;
  }
  else {
    return $conf['taxonomy_section_default'];
  }

  return FALSE;
}

/**
 * Provide a summary description.
 */
function taxonomy_sections_modifier_ctools_access_summary($conf, $context) {
  $term = taxonomy_term_load($conf['taxonomy_section']);
  return t('Taxonomy section term: @taxonomy_section', array('@taxonomy_section' => $term->name));
}
