<?php

/**
 * @file
 * Provides the views data and handlers for taxonomy_sections module.
 * Author : Steven Van den Hout (steven.vandenhout@calibrate.be)
 */

/**
 * Implements hook_views_plugins().
 */
function taxonomy_sections_views_plugins() {
  return array(
    'argument default' => array(
      'taxonomy_sections_purl_tid' => array(
        'title' => t('A term id provided by PURL'),
        'handler' => 'taxonomy_sections_plugin_argument_default_purl_tid',
        'path' => drupal_get_path('module', 'taxonomy_sections') . '/includes',
      ),
    ),
  );
}