<?php
/**
 * @file
 * Settings form for taxonomy sections
 * author: Steven Van den Hout (steven.vandenhout@calibrate.be)
 */

function taxonomy_sections_settings_form() {
  $form = array();
  $options = array();
  $vocs = taxonomy_get_vocabularies();
  foreach ($vocs as $voc) {
    $options[$voc->vid] = $voc->name;
  }
  $form['taxonomy_sections_vocabulary'] = array(
    '#type' => 'select',
    '#title' => t('Taxonomy section vocabulary'),
    '#options' => $options,
    '#default_value' => variable_get('taxonomy_sections_vocabulary'),
  );
  $form['taxonomy_sections_strict'] = array(
    '#type' => 'checkbox',
    '#title' => t('This site has a strict seperation between sections'),
    '#default_value' => variable_get('taxonomy_sections_strict'),
  );

  $options = array();
  $voc = variable_get('taxonomy_sections_vocabulary');
  $terms = taxonomy_get_tree($voc);
  foreach ($terms as $term) {
    $options[$term->tid] = $term->name;
  }
  $form['taxonomy_sections_default_tid'] = array(
    '#type' => 'select',
    '#title' => t('The default term.'),
    '#default_value' => variable_get('taxonomy_sections_default_tid'),
    '#options' => $options,
    '#states' => array(
      'visible' => array(
       ':input[name="taxonomy_sections_strict"]' => array('checked' => TRUE),
      ),
    ),
  );

  return system_settings_form($form);
}