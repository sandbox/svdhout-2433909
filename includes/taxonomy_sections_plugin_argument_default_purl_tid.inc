<?php
/**
 * @file
 *  Return the term id provided from purl
 */

/**
 * Argument default with the purl term id
 */
class taxonomy_sections_plugin_argument_default_purl_tid extends views_plugin_argument_default {

  function get_argument() {
    return taxonomy_sections_get_current_purl_tid();
  }

}